# Simplified Correlation Factor Model


## Meta-GGA exchange-correlation approximation based on a simplified correlation factor 
This repository contains the Python code accompagning the article: Generating Exchange–Correlation Functionals with a Simplified, Self-Consistent Correlation Factor Model

## Description

modelXC.py : to compute density ingredients on the grid.

CFXm.py : to compute the CFBRH XC functional.

BR89fit.py : fit to the Becke-Roussel functional.

## Example Usage
To calculate the energy of atoms:  python calc_E_atoms.py

To calculate atomization energies for a small set of molecules: python calc_atomization.py
